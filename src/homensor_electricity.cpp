#include <ArduinoOTA.h>
#include <PZEM004T.h>
#include <WebSocketsClient.h>
#include <ArduinoJson.h>

#include "config.h" //config


// var
float wattVolt = NAN;
float wattCurrent = NAN;
float wattPower = NAN;
float wattCount = NAN;

int state = 0;

long WaitingClientTime = 10000;
long LastgetWattTime = 0;
long TimeResetWatt = 0;


PZEM004T pzem(&Serial);  // RX,TX
IPAddress ip(192, 168, 1, 1);

WebSocketsClient webSocket;

#define MAX_SRV_CLIENTS 1
WiFiServer telnetServer(23);
WiFiClient telnetServerClients[MAX_SRV_CLIENTS];


void resetWATT() {
    const long now = millis();

    switch (state) {
        case 0: {
            digitalWrite(5, 1);
            TimeResetWatt = now;
            state = 1;
            break;
        }
        case 1: {
            if (now - TimeResetWatt > 6000) {
                digitalWrite(5, 0);
                TimeResetWatt = now;
                state = 2;
            }
            break;
        }
        case 2: {
            if (now - TimeResetWatt > 500) {
                digitalWrite(5, 1);
                TimeResetWatt = now;
                state = 3;
            }
            break;
        }
        case 3: {
            if (now - TimeResetWatt > 1000) {
                digitalWrite(5, 0);
                state = 0;
            }
            break;
        }
        default:
            break;
    }
}

void getWATT() {
    wattVolt = pzem.voltage(ip);
    wattCurrent = pzem.current(ip);
    wattPower = pzem.power(ip);
    wattCount = pzem.energy(ip);
}

void commandAnalizer(uint8_t *message) {
    StaticJsonBuffer<512> jsonBuffer;
    JsonObject &root = jsonBuffer.parseObject(message);
    if (!root.success()) {
        //Serial.println("parseObject() failed");
        return;
    }
    if (root.containsKey("resetCount")) {
        if (root["resetCount"] == true) {
            telnetServerClients->printf("Resetting watt counter...");
            resetWATT();
        }
    }
}

void webSocketEvent(WStype_t type, uint8_t *payload, size_t length) {

    switch (type) {
        case WStype_DISCONNECTED:
            telnetServerClients->printf("[WSc] Disconnected! %s\n", payload);
            break;
        case WStype_CONNECTED: {
            telnetServerClients->printf("[WSc] Connected to url: %s\n", payload);
            // send message to telnetServer when Connected
            //webSocket.sendTXT("Connected");
        }
            break;
        case WStype_TEXT:
            telnetServerClients->printf("[WSc] get text: %s\n", payload);
            commandAnalizer(payload);

            // send message to telnetServer
            webSocket.sendTXT("message here");
            break;
        case WStype_BIN:
            telnetServerClients->printf("[WSc] get binary length: %u\n", length);
            hexdump(payload, length);

            // send data to telnetServer
            // webSocket.sendBIN(payload, length);
            break;
        case WStype_ERROR:
            break;
        case WStype_FRAGMENT_TEXT_START:
            break;
        case WStype_FRAGMENT_BIN_START:
            break;
        case WStype_FRAGMENT:
            break;
        case WStype_FRAGMENT_FIN:
            break;
    }
}


void setupOTA() {
    // No authentication by default
    // ArduinoOTA.setPassword((const char*)"my_password");

    ArduinoOTA.setHostname(WiFi.macAddress().c_str());

    //ArduinoOTA.onStart([]() { Serial.println("OTA start"); });
    //ArduinoOTA.onEnd([]() { Serial.println("\nOTA end"); });
    //ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    //    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    //});
    //ArduinoOTA.onError([](ota_error_t error) {
    //    Serial.printf("Error[%u]: ", error);
    //    switch (error) {
    //        case OTA_AUTH_ERROR:
    //            Serial.println("Auth Failed");
    //            break;
    //        case OTA_BEGIN_ERROR:
    //            Serial.println("Begin Failed");
    //            break;
    //        case OTA_CONNECT_ERROR:
    //            Serial.println("Connect Failed");
    //            break;
    //        case OTA_RECEIVE_ERROR:
    //            Serial.println("Receive Failed");
    //            break;
    //        case OTA_END_ERROR:
    //            Serial.println("End Failed");
    //            break;
    //    }
    //});
}

void setup() {
    Serial.begin(9600);  // serial to pzem
    Serial1.begin(115200);  // virtual console

    WiFi.mode(WIFI_STA);  // Client
    WiFi.begin(ssid, pass);
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
    }

    setupOTA();
    ArduinoOTA.begin();

    telnetServer.begin();
    telnetServer.setNoDelay(true);

    pzem.setAddress(ip);
    pinMode(5, OUTPUT);
    pinMode(4, OUTPUT);

    char url[34];
    snprintf(url, sizeof(url), "/ws/electricity/%s/", WiFi.macAddress().c_str());
    webSocket.begin(wsHost, 80, url);
    webSocket.onEvent(webSocketEvent);
}

void loop() {
    uint8_t i;
    const long now = millis();

    ArduinoOTA.handle();
    webSocket.loop();

    if (state != 0) {
        resetWATT();
    } else {
        if (now - LastgetWattTime > WaitingClientTime) {
            getWATT();
            LastgetWattTime = now;

            StaticJsonBuffer<200> jsonBuffer;
            JsonObject &jroot = jsonBuffer.createObject();
            char jsonMeasurements[200];

            jroot["volt"] = wattVolt;
            jroot["current"] = wattCurrent;
            jroot["power"] = wattPower;
            jroot["count"] = wattCount;
            jroot["sensorid"] = WiFi.macAddress();

            jroot.printTo(jsonMeasurements);
            webSocket.sendTXT(jsonMeasurements);
            telnetServerClients->println(jsonMeasurements);
        }
    }

    if (telnetServer.hasClient()) {
        for (i = 0; i < MAX_SRV_CLIENTS; i++) {
            //find free/disconnected spot
            if (!telnetServerClients[i] || !telnetServerClients[i].connected()) {
                if (telnetServerClients[i]) telnetServerClients[i].stop();
                telnetServerClients[i] = telnetServer.available();
                //Serial.println("New client: ");
                continue;
            }
        }
        //no free/disconnected spot so reject
        WiFiClient serverClient = telnetServer.available();
        serverClient.stop();
    }
}